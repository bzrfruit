# Branch Browser for Bazaar
# Run this with web.py

from os import listdir
from os.path import join, isdir, abspath, expanduser
from datetime import datetime
from configobj import ConfigObj

from bzrlib.branch import BzrBranch
from bzrlib.errors import NoSuchRevision
from bzrlib.osutils import format_date
import bzrlib

from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import TextLexer, guess_lexer_for_filename

import web
web.webapi.internalerror = web.debugerror

from genshi.template import TemplateLoader
from genshi.core import Markup


# Read config file from $HOME
usr_config = ConfigObj(expanduser("~/.bzrfruit.conf"))
config = ConfigObj(dict(web=dict(extra_css="",
                                 skel_template="skel.html"),
                        bzr=dict()))
config.merge(usr_config) # merge with the default values


loader = TemplateLoader(["templates"], auto_reload=True)
def render(name, **namespace):
    "Render the genshi template `name' using `namespace'"
    tmpl = loader.load(name)
    namespace.update(globals()) # we need not pass common methods explicitly
    stream = tmpl.generate(**namespace)
    web.header("Content-Type","%s; charset=utf-8" % "text/html")
    print stream.render('html')

    
# This directory contains the bzr branches
BRANCHES_PATH = expanduser(config["bzr"]["branches_path"])
# $ bzr get BRANCH_URL+nick
BRANCH_URL = config["web"]["branch_url_base"]

WWW_URL = config["web"]["www_url_base"]

def link_to(*segs):
    "Sorry.. this function is difficult to explain. May be some other time."
    # root = web.prefixurl()
    root = WWW_URL
    segs = map(lambda s: str(s).lstrip("/"), segs)
    return root + "/".join(segs)

# http://trac.edgewall.org/browser/trunk/trac/util/datefmt.py
def pretty_timedelta(time1, time2, resolution=None):
    """Calculate time delta (inaccurately, only for decorative purposes ;-) for
    prettyprinting. If time1 is None, the current time is used."""
    if time1 > time2:
        time2, time1 = time1, time2
    units = ((3600 * 24 * 365, 'year',   'years'),
             (3600 * 24 * 30,  'month',  'months'),
             (3600 * 24 * 7,   'week',   'weeks'),
             (3600 * 24,       'day',    'days'),
             (3600,            'hour',   'hours'),
             (60,              'minute', 'minutes'))
    diff = time2 - time1
    age_s = int(diff.days * 86400 + diff.seconds)
    if resolution and age_s < resolution:
        return ''
    if age_s < 60:
        return '%i second%s' % (age_s, age_s != 1 and 's' or '')
    for u, unit, unit_plural in units:
        r = float(age_s) / float(u)
        if r >= 0.9:
            r = int(round(r))
            return '%d %s' % (r, r == 1 and unit or unit_plural)
    return ''

def pretty_size(size):
    jump = 512
    if size < jump:
        return "%d bytes" % size

    units = ['kB', 'MB', 'GB', 'TB']
    i = 0
    while size >= jump and i < len(units):
        i += 1
        size /= 1024.0

    return "%.1f %s" % (size, units[i - 1])


class Branch:

    def __init__(self, url):
        self.branch = BzrBranch.open(url)

    def name(self):
        return self.branch.nick

    def inventory(self):
        "Return the Inventory for last revision"
        b = self.branch
        return b.repository.get_revision_inventory(b.last_revision())

    def revision_tree(self):
        b = self.branch
        return b.repository.revision_tree(b.last_revision())

    
def path2inventory(branch, path):
    # XXX: investigate if this can be done using bzrlib itself
    i = branch.inventory().root
    path = path.strip("/")
    if path == "": return i
    for component in path.split("/"):
        i = i.children[component]
    return i


class BranchPath:

    def __init__(self, branch, path, inventory):
        self.path = path
        self.branch = branch
        self.inventory = inventory
        self.name = inventory.name

    def has_text(self):
        "Usually calls self.inventory.has_text"
        return self.inventory.has_text()

    def revision(self):
        return self.branch.branch.repository.get_revision(
            self.inventory.revision)

    def rev(self):
        try:
            revno = self.branch.branch.revision_id_to_revno(
                self.inventory.revision)
        except NoSuchRevision:
            # FIXME: eek!
            revno = "??" # self.inventory.revision
        return revno, self.inventory.revision

    def age(self):
        r = self.revision()
        dt = datetime.fromtimestamp(r.timestamp)
        return format_date(r.timestamp, r.timezone), \
            pretty_timedelta(dt, datetime.now())

    def demo(self):
        h = self.branch.branch.revision_history()
        last_revid = h[-1]
        r = bzrlib.tsort.merge_sort(
            self.branch.branch.repository.get_revision_graph( ),last_revid)
        l=list([ revid for (seq, revid, merge_depth, end_of_merge) in r ])
        return '\n'.join(l)

    
class BranchDir(BranchPath):
    "Represents a directory in branch"

    def get_children(self):
        children = self.inventory.sorted_children()
        
        # directories first
        for name, inv in children:
            if not inv.has_text():
                yield BranchDir(self.branch, self.path+inv.name+"/", inv)

        # then files
        for name, inv in children:
            if inv.has_text():
                yield BranchFile(self.branch, self.path+inv.name, inv)

            
class BranchFile(BranchPath):
    "Represents a file is branch"

    def content(self):
        tree = self.branch.revision_tree()
        return tree.get_file_text(self.inventory.file_id)

    def highlighted_html(self):
        "Return the pygments highlighted HTML"
        code = self.content()
        try:
            lexer = guess_lexer_for_filename(self.path, code)
        except:
            lexer = TextLexer()
        html = highlight(code, lexer, HtmlFormatter())
        # TODO: this is genshi's bug. It removes double \n in Markup()
        #       since we use <pre>, \n are important.
        #       let's use <br/> as a workaround.
        #       In near future, this must be fixed in genshi.
        html = html.replace('\n', '<br/>')
        return html

            
urls = (
    "/", "branch_list", 
    "/([^/]*)/(.*)",  "branch_browser"
    )


class branch_list:
    def GET(self):
        def available_branches():
            for dir in listdir(BRANCHES_PATH):
                branch_path = join(BRANCHES_PATH, dir)
                # Bazaar branch directory should contain
                # a .bzr sub-directory
                if isdir(join(branch_path, ".bzr")):
                    yield Branch(branch_path)
                    
        render('index.html',
               title="Branches",
               branches=list(available_branches()))
        

class branch_browser:
    def GET(self, name, p):
        p = "/" + p
        b = Branch(join(BRANCHES_PATH, name))
        if p.endswith("/"):
            path = BranchDir(b, p, path2inventory(b, p))
        else:
            path = BranchFile(b, p, path2inventory(b, p))

        # breadcrumb
        breadcrumb = []
        components = path.path.strip('/').split('/')
        cumulative = [b.name()]
        for c in components:
            cumulative.append(c)
            url = link_to(*cumulative) + '/'
            breadcrumb.append([c, url])
        breadcrumb[-1][1] = None # "current" page, so need not link

        render('browser.html',
               title="%s%s" % (b.name(), path.path),
               branch=b,
               branch_url=BRANCH_URL+b.name(),
               path=path,
               breadcrumb=breadcrumb)


